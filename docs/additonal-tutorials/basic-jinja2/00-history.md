## What is Jinja2?
Jinja is a templating engine for developers that uses the Python programming language as base. 
- It's often referred to as "Jinja2". 
- where 2 refers to its release version. In short Flask templating engine is called jinja.

## History of Jinja2
- The original library was created by Armin Ronacher in 2008. 
- Later, the library was used exclusively by web developers.
- Over the years, With the advent of protocols such as NETCONF, the demand for network automation soared. 
- This created a new challenge in the industry. 
- In the end, the industry gravitated towards Jinja2 as the de facto templating language for network configuration.



## If you don't have a computer

If you dont have a computer and using a friend's computer or school lab computer. You can access one of the following sites to experiment with jinja2

- https://repl.it/
- https://colab.research.google.com/

